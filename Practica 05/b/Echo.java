
import java.io.*;
import java.net.*;
import java.util.Scanner;

public class Echo {
  private static EchoObject ss;
  public static void main(String[] args) {
	  /* COMPLETAR: Cree una instància de ss */
	  /* 1 */
	  ss = new EchoObject();
	  /* 1 */
    BufferedReader stdIn= new BufferedReader(new InputStreamReader(System.in));
    PrintWriter stdOut= new PrintWriter(System.out);
    String input,output;
    try {
 
     
/* COMPLETAR: En bucle infinit, llegir del teclat, invocar ss.echo i imprimir */
/* 2 */
      while (true){
    	  stdOut.print("> "); stdOut.flush();
    	  Scanner in=new Scanner(System.in);
    	  input=in.nextLine();
    	  System.out.println(ss.echo(input));
      }
/* 2 */
    } catch (IOException e) {}
  }
}
