package client;
import java.io.*;
import data.*;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
@SuppressWarnings("deprecation")
public class EchoRMI {
  public static void main(String[] args) {
    if (args.length < 1) {
      System.out.println("Us: EchoRMI host");
      System.exit(1);
    }
    if (System.getSecurityManager() == null) {
      System.setSecurityManager(new RMISecurityManager());
    }
    BufferedReader stdIn= new BufferedReader(new InputStreamReader(System.in));
    PrintWriter stdOut= new PrintWriter(System.out);
    String input,output;
    try {

    	EchoInt obj= (EchoInt) Naming.lookup( "rmi://localhost/"+"echo");
      stdOut.print("> ");
      stdOut.flush();
      while ((input= stdIn.readLine()) != null) {
    	output = obj.echo(input);
        stdOut.println(output);
        stdOut.print("> "); stdOut.flush();
      }
    } catch(Exception e) {
      System.out.println("Error en client echo RMI: " + e.getMessage());
    }
  }
}
