// HolaMonRmiO.java
/**
 * Aquest és l'objecte remot per a la version RMI del missatge
 * de salutació "Hola Món"
 */
package upv.dad.dap03.partb.servidor;
import upv.dad.dap03.partb.rmi.HolaMonRmiI;

import java.rmi.*;
import java.rmi.server.*;
public class HolaMonRmiO extends UnicastRemoteObject
implements HolaMonRmiI {
  public HolaMonRmiO() throws RemoteException { // constructor
    // super(); // certament aquesta línia no és necessària (per defecte)
  }
  public String objRemotHola(String client) throws RemoteException {
    return("Hola "+client);
  }
}
