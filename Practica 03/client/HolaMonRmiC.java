
// HolaMonRmiC.java
/**
 * Aquest programa és el client per a la version RMI del missatge
 * de salutació "Hola Món"
 */
package upv.dad.dap03.partb.client;
import upv.dad.dap03.partb.rmi.HolaMonRmiI;

import java.rmi.*;
public class HolaMonRmiC {
  public static void main (String[] args) {
    System.setProperty("java.security.policy", "holamon.policy");
    System.setSecurityManager(new RMISecurityManager());
    // adreça de la màquina remota, en aquest cas la mateixa màquina local
    // si s'executarà en una màquina diferent, hauria de canviar-se
    // a quelcom semblant a: "rmi://servidor.com/"
    String urlRemot = "rmi://158.42.51.185/"; 
    try {
      HolaMonRmiI hm= (HolaMonRmiI)Naming.lookup(urlRemot+"holastring");
      System.out.println(hm.objRemotHola("Món"));
    } catch(Exception e) {
      e.printStackTrace();
    }
  }
}
