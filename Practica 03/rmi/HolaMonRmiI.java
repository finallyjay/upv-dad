// HolaMonRmiI.java
/**
 * Aquesta és la interfície que implementa l'objecte remot per a la version RMI 
 * del missatge de salutació "Hola Mon"
 */
package upv.dad.dap03.partb.rmi;

import java.rmi.*;
public interface HolaMonRmiI extends Remote {
  String objRemotHola(String client) throws RemoteException;
}
