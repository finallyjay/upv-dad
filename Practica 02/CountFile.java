package hola;
import java.io.*;

public class CountFile {
  public static void main (String[] args)
      throws java.io.IOException, java.io.FileNotFoundException {
    int count= 0;
    String filename = null;
    FileInputStream fis = null;
    char current;
    if (args.length >= 1) {
/* COMPLETAR: is= Cree una instància de FileInputStream
 * per a llegir del fitxer passat com argument a args[0]
 */
/* 1 */
    	filename = args[0];
    	File fitxer = new File(filename);
    	fis = new FileInputStream(fitxer);
/* 1 */
      
    } else {
      filename= "Input";
    }
/* COMPLETAR: Utilitze amb while (...) count++;
 * un mètode de FileInputSream per a llegir un caracter
 */
/* 2 */
    while (fis.available() > 0) {
        fis.read();
        count++;
      }
/* 2 */

    fis.close();
    System.out.println(filename + " has " + count + " chars.");
  }
}
